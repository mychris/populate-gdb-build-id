# populate-gdb-build-id

A small CLI utility which creates a `.build-id` directory, containing debug
information in the form expected by GDB.
See the GDB
[documentation](https://sourceware.org/gdb/onlinedocs/gdb/Separate-Debug-Files.html)
for more information about the format.

`populate-gdb-build-id` reads the build-id using `readelf` from each provided
`.debug` file and creates a symbolic link in the `.build-id` directory to it.
Inside GDB, one has to configure the debugger to use these debugging symbols
by configuring the setting `debug-file-directory`.

This is especially useful during a remote debugging session, if the application
is big and loading the debugging symbols from the remote machine takes too long.

In order to have the `build-id` available in your application, the flag
`--build-id` needs to be given to the linker. For `gcc`, `-Wl,--build-id` needs
to be given to the compiler driver.

See the help of the utility for more information.

## Help

```
NAME
    populate-gdb-build-id - Populate the GDB debug-file-directory using build-ids.

SYNOPSIS
    populate-gdb-build-id [-kC] [-cml] [-v] [-h] debug-file-directory debug-files...

DESCRIPTION
    For each given debug-file, inspect its build-id using the readelf tool
    and populate GDBs .build-id directory in the given debug-file-directory
    with the debug information.

    By default, the .build-id directory will be cleaned prior to populating it.
    This way, one can always execute the same command once after every re-deploy
    of the software without the need to manually handle the debug-file-directory
    ever.

    By default, populate-gdb-build-id only creates symbolic links to the
    given debug-files. If the -c option is given, the debug files will be copied,
    or, with the -m option, moved.

    To use the debug files inside GDB, use the "set debug-file-directory" command
    with the same path as given to populate-gdb-build-id.

    See the GDB documentation about "Debugging Information in Separate Files"
    for more information about the .build-id directory and the debug-file-directory
    setting.

OPTIONS
    -k, --keep
           Keep contents of the .build-id directory intact and only add new
           files. If required, old files will be overridden.

    -C, --clean
           Clean contents of the .build-id prior to adding new files.

    -c, --copy
           Copy the debug files into the .build-id directory.

    -m, --move
           Move the debug files into the .build-id directory.

    -l, --link
           Create a symbolic link to the debug files in the .build-id directory.

    -v, --verbose
           Be verbose during operation.

    -h, --help
           Display this help and exit.

EXAMPLES
    Populate for a whole project, using extended globs of the shell

        $ populate-gdb-build-id /tmp/dbg /path/to/project/**/*.debug

    Populate for a whole project, using find and xargs

        $ find /path/to/project -name '*.debug' -print0 |xargs -0 populate-gdb-build-id /tmp/dbg

    The files containing the debug information do not have to have the debug file extension.
    GDB documents this to be the usual file extension for debugging information.
```

## Copyright

Copyright © 2021-2022, Christoph Göttschkes.

Released under MIT license.
